# Cognac

This is a lightweight gem for implementing Amazon S3 upload using CORS support. The dependencies are kept to a minimum for easier maintenance. 

This gem builds the AWS authentication signature as described in http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html. It requires the Amazon keys, as well as options about the request (request type, resource, etc).

To experiment with this code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cognac'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cognac

## Dependencies

Ruby 2.2.3
ActiveSupport 4.2.3

## Usage

Configure the AWS credentials in config/initializers/aws.rb:

```ruby
  Cognac.configuration do |config|
    config.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
    config.aws_access_key    = ENV['AWS_ACCESS_KEY_ID']
	config.aws_s3_bucket     = ENV['AWS_S3_BUCKET']
  end
```

You can use this gem in your Rails controller like this:

```ruby
  def generate_signed_s3_url
    cloud_file = Cognac::CloudFile.new(params[:filename])
    resource_end_point = cloud_file.resource_end_point
	
    options = Cognac::Signature.generate_options_for_build_s3_upload_url(cloud_file.name, params[:content_type])
    url = Cognac::Signature.build_s3_upload_url(resource_end_point, options)
    
    render :json => {put_url: url, file_url: resource_end_point}
  end
```

For a sample Rails project that shows how to use this gem, check out: https://github.com/bparanj/s3-cors-upload-rails

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Run the tests:

```sh
$ruby -rminitest/pride test/cloud_file_test.rb  --verbose
$ruby -rminitest/pride test/signature_test.rb  --verbose
```

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/bparanj/cognac.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).