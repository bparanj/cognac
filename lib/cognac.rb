require "cognac/cloud_file"
require "cognac/signature"
require "cognac/version"
require 'lyon'

module Cognac
  extend Lyon::Configuration

  # Amazon Credentials
  define_setting :secret_access_key
  define_setting :aws_access_key
  define_setting :aws_s3_bucket
end