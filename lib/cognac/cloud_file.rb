require 'securerandom'

module Cognac
  class CloudFile
    
    def initialize(file_name)
      @file_name = file_name
    end
    
    # To avoid file collision, we prepend random string to the file_name
    def name
      "#{SecureRandom.hex(4).to_s}_#{@file_name}"
    end
    
    def resource_end_point
      raise 'AWS_S3_BUCKET Environment variable is not intialized. Refer README.md at https://bitbucket.org/bparanj/cognac' if Cognac.aws_s3_bucket.nil?
      "http://#{Cognac.aws_s3_bucket}.s3.amazonaws.com/#{name}"
    end
    
  end
  
end