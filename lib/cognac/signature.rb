require 'active_support/core_ext/numeric/time'
require "erb"

module Cognac
  
  # file = Cognac::CloudFile.new(params[:filename], bucket)
  #
  # resource_end_point = file.resource_end_point
  # url = file.upload_url(ENV["AWS_ACCESS_KEY_ID"], ENV["AWS_SECRET_ACCESS_KEY"], params[:content_type])
  
  # 1. Create cloudfile
  # 2. Generate end point
  # 3. Generate upload url
    
  # Input
  # - file_name
  # - bucket
  # - access_key_id
  # - secret_access_key
  # - content_type
  #
  # Output
  # - Signed S3 Upload URL
  
  # build_s3_upload_url (access_key, secret_access_key)
  #  - Encode signature
  #    - build s3 rest signature (secret_access_key)
  #       - build_string_to_sign (options)
  #       - base encode hmac digest (string to sign, secret_access_key)
  #  - End point
  
  # AWS Signature Version 2 : http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html
  class Signature
    NEW_LINE = "\n"
    SECURE_HASH_ALGORITHM = "sha1"
    BLANK_STRING = ""
    UTF8_ENCODING = "UTF-8"
    HTTP_PUT = "PUT"
    
    def initialize(options = {})
      if Cognac.secret_access_key.nil?
        raise 'AWS Secret access key is not initialized. Refer README.md at https://bitbucket.org/bparanj/cognac'
      end

      if Cognac.aws_access_key.nil?
        raise 'AWS access key is not initialized. Refer README.md at https://bitbucket.org/bparanj/cognac'
      end
      
      @secret_access_key = Cognac.secret_access_key
      @aws_access_key = Cognac.aws_access_key
      @options = options
      @default = {
        http_verb:     HTTP_PUT,
        content_md5:   nil,
        content_type:  nil,
        date:          an_hour_from_now,
        amz_headers:   [],
        resource:      BLANK_STRING
      }
    end
 
    # This is called in the controller
    def build_s3_upload_url(end_point)
      "#{end_point}?AWSAccessKeyId=#{@aws_access_key}&Expires=#{expiration}&Signature=#{encoded_signature}"     
    end
    
    def encoded_signature
      ERB::Util.url_encode(build_s3_rest_signature)
    end

    def build_s3_rest_signature
       string_to_sign = build_string_to_sign(@options).force_encoding(UTF8_ENCODING)
       base_encoded_hmac_digest(string_to_sign)
    end
    
    def build_string_to_sign(options = {})
      o = @default.merge(options)

      add_new_line(o[:http_verb]) +
      add_new_line(o[:content_md5]) +
      add_new_line(o[:content_type]) +
      add_new_line(o[:date]) +
      (o[:amz_headers].any? ? (o[:amz_headers].join(NEW_LINE) + NEW_LINE) : BLANK_STRING) +
      o[:resource]
   end
    
   def base_encoded_hmac_digest(string_to_sign)
     Base64.encode64(OpenSSL::HMAC.digest(SECURE_HASH_ALGORITHM, @secret_access_key, string_to_sign)).strip
   end
   
   # This is called in the controller and passed in to the build_s3_upload_url method as a parameter
   # Hide this method
   def generate_options_for_build_s3_upload_url(file_name, content_type)
     if Cognac.aws_s3_bucket.nil?
       raise 'AWS S3 Bucket is not initialized. Refer README.md at https://bitbucket.org/bparanj/cognac' 
     end
     {
        http_verb:    HTTP_PUT, 
        date:         1.hours.from_now.to_i, 
        resource:     "/#{Cognac.aws_s3_bucket}/#{file_name}",
        content_type: content_type
     }
   end
   
   private
   
   def add_new_line(option)
     option.to_s + NEW_LINE
   end
   
   def an_hour_from_now
     1.hours.from_now.rfc822
   end
   
   def expiration
     @options[:date] || an_hour_from_now
   end
   
  end
end