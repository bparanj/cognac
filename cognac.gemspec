# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cognac/version'

Gem::Specification.new do |spec|
  spec.name          = "cognac"
  spec.version       = Cognac::VERSION
  spec.authors       = ["Bala Paranj"]
  spec.email         = ["bparanj@gmail.com"]

  spec.summary       = %q{A simple library to implement Amazon S3 upload using CORS.}
  spec.description   = %q{This gem builds the AWS authentication signature as described in http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html. It requires the Amazon keys, as well as options about the request (request type, resource, etc).}
  spec.homepage      = "http://www.rubyplus.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  
  spec.required_ruby_version = '>= 2.2.2'
  
  spec.add_dependency 'activesupport', '~> 4.2'
  spec.add_dependency 'lyon', "~> 0.1"
  
  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.7"
end
