$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'cognac'

require 'minitest/autorun'

ENV['AWS_S3_BUCKET'] = 'rubyplus.com'
ENV['AWS_SECRET_ACCESS_KEY'] = 'bogus-secret' 
ENV['AWS_ACCESS_KEY_ID'] = 'bogus-access_key'

def configure_gem
  Cognac.configuration do |config|    
    config.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
    config.aws_access_key    = ENV['AWS_ACCESS_KEY_ID']
    config.aws_s3_bucket     = ENV['AWS_S3_BUCKET']
  end
end
