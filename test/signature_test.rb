require_relative 'test_helper'
require 'cognac/signature'

class SignatureTest < Minitest::Test
  include Cognac
  
  HTTP_GET = 'GET'
  HTTP_PUT = 'PUT'
  
  def setup
    configure_gem
  end
  
  def test_build_s3_string_to_sign_without_headers
    expected_result = "#{HTTP_GET}\n\n\nTue, 27 Mar 2007 19:44:46 +0000\n/johnsmith/?acl"  

    signature = Signature.new
    result = signature.build_string_to_sign(http_verb: HTTP_GET, date: 'Tue, 27 Mar 2007 19:44:46 +0000', resource: '/johnsmith/?acl')
    
    assert_equal expected_result, result
  end
  
  def test_build_s3_string_to_sign_with_amazon_headers
    expected_result = "PUT\n4gJE4saaMU4BqNR0kLY+lw==\napplication/x-download\nTue, 27 Mar 2007 21:06:08 +0000\nx-amz-acl:public-read\nx-amz-meta-checksumalgorithm:crc32\n/static.johnsmith.net/db-backup.dat.gz"

    signature = Signature.new
    result = signature.build_string_to_sign(http_verb:    HTTP_PUT,
                                            date:         "Tue, 27 Mar 2007 21:06:08 +0000",
                                            content_md5:  "4gJE4saaMU4BqNR0kLY+lw==",
                                            content_type: "application/x-download",
                                            resource: "/static.johnsmith.net/db-backup.dat.gz",
                                            amz_headers: ["x-amz-acl:public-read", "x-amz-meta-checksumalgorithm:crc32"])

    assert_equal expected_result, result
  end

  def test_build_s3_rest_signature
    options = {
      http_verb: HTTP_GET,
      date:      "Tue, 27 Mar 2007 19:36:42 +0000",
      resource:  "/johnsmith/photos/puppy.jpg"
    }
    expected_result = "eKjs5/dHP8yuO15wro1eyZQIgts="

    signature = Signature.new(options)
    result = signature.build_s3_rest_signature

    assert_equal expected_result, result 
  end

  def test_generate_options_for_build_s3_upload_url
    signature = Signature.new
    result = signature.generate_options_for_build_s3_upload_url('tdd.pdf', 'pdf')

    expected_result = {
      http_verb: HTTP_PUT,
      date: 1.hours.from_now.to_i,
      resource: "/rubyplus.com/tdd.pdf",
      content_type: 'pdf'
    }

    assert_equal expected_result, result
  end

  def test_build_s3_upload_url
    options = {
      http_verb: HTTP_GET,
      date:      "1175139620",
      resource:  "/johnsmith/photos/puppy.jpg"
    }
    secret_key = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
    access_key = "AKIAIOSFODNN7EXAMPLE"

    signature = Signature.new(options)
    result = signature.build_s3_upload_url("/photos/puppy.jpg")

    expected_result = "/photos/puppy.jpg?AWSAccessKeyId=bogus-access_key&Expires=1175139620&Signature=raQuf5YfptNGOjvbd3hC2tsHrLM%3D"

    assert_equal expected_result, result
  end
  
  def test_signature_raises_exception_if_secret_access_key_is_missing
    Cognac.configuration do |config|    
      config.secret_access_key = nil
    end
    
    assert_raises Exception do
      Signature.new
    end
  end

  def test_signature_raises_exception_if_aws_access_key_is_missing
    Cognac.configuration do |config|    
      config.aws_access_key = nil
    end
    
    assert_raises Exception do
      Signature.new
    end
  end
  
  def test_generate_options_for_build_s3_upload_url_raises_exception_if_aws_s3_bucket_is_not_initialized
    Cognac.configuration do |config|    
      config.aws_s3_bucket = nil
    end
    
    assert_raises Exception do
      signature = Signature.new
      signature.generate_options_for_build_s3_upload_url('tdd.pdf', 'pdf')
    end
  end
    
end