require_relative 'test_helper'
require 'cognac/cloud_file'

class CloudFileTest < Minitest::Test
  include Cognac

  def test_generate_s3_file_name_to_avoid_file_collision
    file = CloudFile.new('a')
    
    result = file.name.size
    
    assert_equal 10, result 
  end

  # Assuming bucket name is rubyplus.com
  # Return value : "http://rubyplus.com.s3.amazonaws.com/a7a345e2_tdd.pdf"
  def test_s3_resource_end_point
    configure_gem
    file_name = 'tdd.pdf'
    
    file = CloudFile.new(file_name)
    file.stub :name, 'a7a345e2_tdd.pdf' do
      result = file.resource_end_point

      assert_equal 'http://rubyplus.com.s3.amazonaws.com/a7a345e2_tdd.pdf', result
    end
  end
  
  def test_s3_resource_end_point_raises_exception_when_aws_s3_bucket_is_not_initialized
    Cognac.configuration do |config|    
      config.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
      config.aws_access_key    = ENV['AWS_ACCESS_KEY_ID']
      config.aws_s3_bucket     = nil
    end
    
    file_name = 'tdd.pdf'
    
    file = CloudFile.new(file_name)
    file.stub :name, 'a7a345e2_tdd.pdf' do
      
      assert_raises Exception do
        file.resource_end_point
      end
    end
  end
  
end