
== Version 0.1.3 (July 1, 2015)

Old Interface

    file_name = Cognac::CloudFile.generate(params[:filename])   
    resource_end_point = Cognac::CloudFile.resource_end_point(ENV["AWS_S3_BUCKET"], file_name) 
   
New Interface

    file = Cognac::CloudFile.new(params[:filename], bucket)  
    resource_end_point = file.resource_end_point

== In Progress

Old Interface

    options = Cognac::Signature.generate_options_for_build_s3_upload_url(ENV["AWS_S3_BUCKET"], file_name, params[:content_type])   
    url = Cognac::Signature.build_s3_upload_url(resource_end_point, ENV["AWS_ACCESS_KEY_ID"], ENV["AWS_SECRET_ACCESS_KEY"], options)

New Interface

    url = Cognac::CloudFile.upload_url(ENV["AWS_ACCESS_KEY_ID"], ENV["AWS_SECRET_ACCESS_KEY"], params[:content_type])
  
== Combining the Two

    file = Cognac::CloudFile.new(params[:filename], bucket)  
    
    resource_end_point = file.resource_end_point
    url = file.upload_url(ENV["AWS_ACCESS_KEY_ID"], ENV["AWS_SECRET_ACCESS_KEY"], params[:content_type])
    
    render :json => {put_url: url, file_url: resource_end_point}
    

Separate the configuration values for object properties.